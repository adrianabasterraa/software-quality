
test("Valid", function (assert) {
    assert.equal(isValidPlate('1234DDD'), VALID, "UPPERCASE");
    assert.equal(isValidPlate('5634dfh'), VALID, "LOWERCASE");
    assert.equal(isValidPlate('9999rrr'), VALID, "SAME DIGITS & LETTERS");
});

test("Invalid", function (assert) {
    assert.equal(isValidPlate('000DDD'), INVALID, "MISSING ONE NUMBER");
    assert.equal(isValidPlate('7866G'), INVALID, "TWO LETTERS MISSING");
    assert.equal(isValidPlate(''), INVALID, "ALL CHARACTERS MISSING");
    assert.equal(isValidPlate('1234QET'), INVALID, "INCLUDES Q");
    assert.equal(isValidPlate('1234ETÑ'), INVALID, "INCLUDES Ñ");
    assert.equal(isValidPlate('1234AEI'), INVALID, "HAS VOWELS");
});

test("Not a Plate", function (assert) {
    assert.equal(isValidPlate(), NOT_A_PLATE, "NULL");
    assert.equal(isValidPlate(1231), NOT_A_PLATE, "NOT A STRING - NUMBER");
    assert.equal(isValidPlate(Object()), NOT_A_PLATE, "NOT A STRING - OBJECT");
});