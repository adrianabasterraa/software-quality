INVALID_ARGUMENTS = "The arguments were not valid";
NOT_A_PLATE = "Not a plate";
VALID = true;
INVALID = false;

/*
 * Given a plate it calculates if it is valid or not.
 */
function isValidPlate(plate) {
  if(plate == null || typeof plate !== 'string'){
    return NOT_A_PLATE;
  }
  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;

  if(plate.match(re) != null) {
    return VALID;
  }
  else {
    return INVALID;
  }
}